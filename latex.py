import json
import os
import shutil

def write_cv():
    json_folder = os.path.join(os.path.dirname(__file__), 'frontend/json')

    try:
        data = {}
        for json_name in ['info', 'education', 'work', 'project']:
            f = open(os.path.join(json_folder, json_name+'.json'))
            data[json_name] = json.load(f)
            f.close()
    except:
        raise Exception("Unable to load metadata")
    
    cv = open('cv.tex', 'a')

    # Personal Info
    cv.write(annotate_section('Begin document'))
    cv.write("\\name{{\color{{navyblue}} {name}}}\n".format(name=data['info']['name']))
    cv.write("\n\\begin{document}\n\n")
    cv.write("\printPersonalInfo{\n")
    cv.write(" \personalInfo{{\info{{{phone}}}\,\\textbar\,\info{{{email}}}\,\\textbar\,\info{{{linkedin}}}}}\n".format(phone=data['info']['phone'], email=data['info']['email'], linkedin=data['info']['linkedin']))
    cv.write("}\n\n")

    # Education
    section_name = "Education"
    cv.write(annotate_section(section_name))
    cv.write(begin_section(section_name))
    for edu in data['education']:
        content = "\\begin{rSubsectionNoBullet}"
        content += "{{\\bf {degree}}}".format(degree=edu['degree'])
        for item in ['location', 'school', 'dates']:
            content += "{{{item}}}".format(item=edu[item])
        if len(edu['add-on']) > 0:
            content += "\n"
        for add in edu['add-on']:
            content += "\italicitem{{{add}}}".format(add=add)
        content+="\\end{rSubsectionNoBullet}\n"
        cv.write(content)
    cv.write(end_section())

    # Work Experience
    section_name = "Work Experience"
    cv.write(annotate_section(section_name))
    cv.write(begin_section(section_name))
    for work in data['work']['cv-only']:
        content = "\\begin{rSubsection}"
        for item in ['position', 'dates', 'company', 'location']:
            content += "{{{item}}}".format(item=work[item])
        if len(work['duties']) > 0:
            content += "\n"
        for duty in work['duties']:
            content += "\item {duty}\n".format(duty=duty)
        content+="\\end{rSubsection}\n"
        cv.write(content)
    cv.write(end_section())

    # Projects
    section_name = "Projects"
    cv.write(annotate_section(section_name))
    cv.write(begin_section(section_name))
    for project in data['project']['cv-only']:
        content = "\\begin{rSubsection}"
        for item in ['name', 'role', 'link', '']:
            if item in project:
                content += "{{{item}}}".format(item=project[item])
            else:
                content += "{}"
        if len(project['add-on']) > 0:
            content += "\n"
        for add in project['add-on']:
            content += "\item {add}\n".format(add=add)
        content+="\\end{rSubsection}\n"
        cv.write(content)
    cv.write(end_section())



    cv.write("\n\end{document}")
    cv.close()


def annotate_section(section_name):
    doc_line = '%----------------------------------------------------------------------------------------\n'
    return doc_line + "%   {}\n".format(section_name) + doc_line + '\n'

def begin_section(section_name):
    return "\\begin{{rSection}}{{{section_name}}}\n\n".format(section_name=(section_name.upper()))

def end_section():
    return "\n\end{rSection}\n\n"

if __name__ == '__main__':
    shutil.copyfile('template.tex', 'cv.tex')
    write_cv()
