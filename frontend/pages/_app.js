import Layout from '../components/layout'
import { ChakraProvider } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import theme from '../theme'
import Head from 'next/head'
import { basePath } from '../next.config'

export default function MyApp({ Component, pageProps }) {
  const router = useRouter()
  return (
    < ChakraProvider theme={theme}>
      <Head>
        <link rel="icon" href={`${basePath}/favicon.ico`} />
      </Head>
      <Layout path={router.asPath}>
        <Component {...pageProps} />
      </Layout>
    </ChakraProvider>
  )
}