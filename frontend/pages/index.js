import { Image, Stack, Box, StackDivider, SimpleGrid } from "@chakra-ui/react"
import { basePath } from "../next.config"

export default function Home() {
  return (
    <Stack
      direction={'column'}
      flex='1'
    >
      <Box>
      </Box>

      <SimpleGrid
        columns={[1, 1, 2]}
        spacing={10}
      >
        <Box
          justifySelf='center'
          boxShadow='inset 0px 4px 4px rgba(0, 0, 0, 0.3)'
          borderRadius='50%'
          boxSize={['100px', '200px', '350px']}
          backgroundImage={`${basePath}/avatar.jpg`}
          backgroundSize='115%'
          backgroundPosition='center'
          alt='Queenie Lee'
        >
        </Box>
        <Box>
        </Box>
      </SimpleGrid>

    </Stack>
  )
}