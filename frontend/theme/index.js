import { extendTheme } from '@chakra-ui/react'
import "@fontsource/courier-prime"

const overrides = {
  components: {
    Button: {
      baseStyle: {
        fontFamily: 'Courier Prime',
        fontWeight: 'normal',
        color: 'white',
        borderRadius: '45px',
        iconSpacing: 1,
        _hover: {
          bg: '#5C5C5C'
        },
        _active: {
          boxShadow: 'inset 0px 4px 5px rgba(0, 0, 0, 0.3)',
          bg: '#FF8C00'
        }
      },
      sizes: {
        sm: {
          fontSize: '6px',
          border: '0.25px solid #FFFFFF',
          w: '50px',
          h: '15px'
        },
        md: {
          fontSize: '16px',
          w: '130px',
          h: '30px'
        },
        lg: {
          fontSize: '28px',
          w: '215px',
          h: '60px'
        },
        icon: {
          w: 'fit-content',
          h: 'fit-content'
        }
      },
      variants: {
        'selected': {
          bg: '#FFA500 !important',
          boxShadow: '0px 4px 5px rgba(0, 0, 0, 0.2) !important',
          borderWidth: '0 !important'
        },
        'icon': {
          bg: 'transparent',
          _hover: {
            bg: 'transparent'
          },
          _active: {
            bg: 'transparent',
            boxShadow: '',
          }
        }
      },
      defaultProps: {
        size: ['sm', 'md', 'lg'],
        variant: 'non-selected'
      }
    }

  }
}

export default extendTheme(overrides)