# Queenie's Personal Website

Initial app via starter tempalte: [Learn Next.js](https://nextjs.org/learn).
`npx create-next-app@latest queenie-web --use-npm --example "https://github.com/vercel/next-learn/tree/master/basics/learn-starter"`

Adopt [Chakra UI](https://chakra-ui.com/docs/components)
`npm i @chakra-ui/react @emotion/react @emotion/styled framer-motion`


## References
[Compiling LaTeX documents with GitLab CI](https://gitlab.com/islandoftex/images/texlive/-/wikis/Compiling-LaTeX-documents-with-GitLab-CI)
[PyLaTex API reference](https://jeltef.github.io/PyLaTeX/current/api.html)