import Header from "./header";
import Footer from "./footer";
import { Flex, Stack, Center, Square, Text, Box } from "@chakra-ui/react";

export default function Layout({ path, children }) {
  return (
    <Flex>
      <Header path={path} />
      <Flex
        bg='white'
        // w='full'
        position='fixed'
        top={['50', '0']}
        bottom={['50', '100']}
        right='0'
        left={['0', '175', '300']}
        pb={['75', '125']}
        overflowY='auto'
        px={[3, 6, 20]}
        pt={[3, 6, 10]}
      >
        {children}
      </Flex>
      <Footer />
    </Flex>
  )
}