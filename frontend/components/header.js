import { Button, Flex, Icon, LinkOverlay, Stack } from "@chakra-ui/react";
import NextLink from 'next/link'
import { basePath } from "../next.config";
import DownloadCV from "./downloadcv";

export default function Header({ path }) {
	return (
		<Flex
			bg='#008080'
			direction={['row', 'column']}
			h={['50px', '100%']}
			w={['100vw', '175px', '300px']}
			position='fixed'
			align='center'
			backgroundImage={[`${basePath}/mLogo.png`, `${basePath}/dLogo.png`]}
			backgroundSize='contain'
			backgroundRepeat='no-repeat'
			overflowY='auto'
			justifyContent={['end', 'normal']}
		>
			<Stack
				direction={['row', 'column']}
				spacing={[4, 16]}
				pt={[0, '250px', '340px']}
				pr={[5, 0]}
			>
				<Button
					variant={path == '/' && 'selected'}
				>
					<LinkOverlay as={NextLink} href='/'>Home</LinkOverlay>
				</Button>

				<Button
					variant={path == '/about' && 'selected'}
				>
					<LinkOverlay as={NextLink} href='/about'>About</LinkOverlay>
				</Button>

				<Button
					variant={path == '/experience' && 'selected'}
				>
					<LinkOverlay as={NextLink} href='/experience'>Experience</LinkOverlay>
				</Button>

				<DownloadCV display={['none', 'flex']} text={'CV'} />
			</Stack>
		</Flex>
	);
};


export const DownloadIcon = (props) => (
	<Icon viewBox='0 0 32 30' {...props}>
		<path d="M16.8728 26.162V12.0294H14.7637V26.162L11.3442 22.9115L9.85278 24.3291L15.8182 29.9999L21.7836 24.3291L20.2923 22.9115L16.8728 26.162Z" fill="currentColor" />
		<path d="M25.3091 9.0221C25.3091 4.04742 21.0514 0 15.8182 0C10.585 0 6.32727 4.04742 6.32727 9.0221C4.64918 9.0221 3.03981 9.6558 1.85322 10.7838C0.666621 11.9118 0 13.4416 0 15.0368C0 16.632 0.666621 18.1619 1.85322 19.2899C3.03981 20.4179 4.64918 21.0516 6.32727 21.0516H11.6V19.0467H6.32727C5.20854 19.0467 4.13563 18.6242 3.34457 17.8722C2.5535 17.1202 2.10909 16.1003 2.10909 15.0368C2.10909 13.9734 2.5535 12.9535 3.34457 12.2015C4.13563 11.4495 5.20854 11.027 6.32727 11.027H8.43636V9.0221C8.43636 7.16103 9.21409 5.37618 10.5984 4.0602C11.9828 2.74422 13.8604 2.00491 15.8182 2.00491C17.776 2.00491 19.6536 2.74422 21.0379 4.0602C22.4223 5.37618 23.2 7.16103 23.2 9.0221V11.027H25.3091C26.4278 11.027 27.5007 11.4495 28.2918 12.2015C29.0829 12.9535 29.5273 13.9734 29.5273 15.0368C29.5273 16.1003 29.0829 17.1202 28.2918 17.8722C27.5007 18.6242 26.4278 19.0467 25.3091 19.0467H20.0364V21.0516H25.3091C26.9872 21.0516 28.5966 20.4179 29.7831 19.2899C30.9697 18.1619 31.6364 16.632 31.6364 15.0368C31.6364 13.4416 30.9697 11.9118 29.7831 10.7838C28.5966 9.6558 26.9872 9.0221 25.3091 9.0221Z" fill="currentColor" />
	</Icon>
)